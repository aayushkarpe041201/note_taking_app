import PropTypes from "prop-types";
import { useEffect, useState } from "react";

import { Link } from "react-router-dom";

import lb from "../css/LandingBox.module.css";

function LandingBox({ handleOnClickNewGrpBox }) {
  const [infos, setInfos] = useState([]);
  const [selectedLink, setSelectedLink] = useState(
    window.location.href.split("/").slice(-1)[0] || null
  );

  const handlelbCRGC = (e) => {
    e.stopPropagation();
    handleOnClickNewGrpBox(true);
  };

  useEffect(() => {
    if (!infos || JSON.stringify(infos) !== localStorage.getItem("Data")) {
      const data = localStorage.getItem("Data");

      if (data) {
        const parsedData = JSON.parse(data);
        setInfos(parsedData);
      }
    }
  }, [handleOnClickNewGrpBox, infos]);

  const handleLinkClick = (infoId) => {
    setSelectedLink(infoId);
  };

  return (
    <div id={lb.mainBoxLb}>
      <Link
        to={"/"}
        id={lb.lbheading}>
        Pocket Notes
      </Link>

      <div
        id={lb.lbCRGC}
        onClick={handlelbCRGC}>
        + Create Notes group
      </div>

      <div id={lb.lbGrpList}>
        {infos.map((info) => (
          <Link
            to={`/Chat/${info.id}`}
            key={info.id}
            className={lb.lbGrpBox}
            onClick={() => handleLinkClick(info.id)}
            style={{
              backgroundColor: selectedLink === info.id ? "#f7ecdc" : "#FFFFFF",
            }}>
            {
              <div
                className={lb.lbGrpBoxCircle}
                style={{
                  backgroundColor: info.color,
                  borderRadius: "50%",
                  height: "3rem",
                  width: "3rem",
                  margin: "2rem",
                  textAlign: "center",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  justifySelf: "flex-start",
                  marginLeft: "1.5rem",
                  fontSize: "1.2rem",
                  color: "#FFFFFF",
                }}>
                {info.name
                  .split(" ")
                  .map((w) => w[0])
                  .join("")
                  .toUpperCase()}
              </div>
            }{" "}
            <p className={lb.lbGrpBoxP}>{info.name}</p>
          </Link>
        ))}
      </div>
    </div>
  );
}

LandingBox.propTypes = {
  handleOnClickNewGrpBox: PropTypes.func.isRequired,
};

export default LandingBox;
