import { useEffect, useState } from "react";
import mlp from "../css/MobileLandingPage.module.css";
import { Link } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";

function MobileLandingPage() {
  const [blur, setBlur] = useState(false);
  const [InpBorder, setInpBorder] = useState(true);

  const [formData, setFormData] = useState({
    name: "",
    color: "",
  });

  const colorSelectArray = [
    "#B38BFA",
    "#FF79F2",
    "#43E6FC",
    "#F19576",
    "#0047FF",
    "#6691FF",
  ];

  const [infos, setInfos] = useState([]);

  useEffect(() => {
    if (!infos || JSON.stringify(infos) !== localStorage.getItem("Data")) {
      const data = localStorage.getItem("Data");

      if (data) {
        const parsedData = JSON.parse(data);
        setInfos(parsedData);
      }
    }
  }, [infos]);

  const handleFormSubmit = () => {
    const existingData = localStorage.getItem("Data");

    const parsedData = existingData ? JSON.parse(existingData) : [];

    const duplicateObject = parsedData.find(
      (obj) => obj.name === formData.name
    );

    if (duplicateObject) {
      setInpBorder(false);
    } else {
      const uniqueKey = uuidv4();

      const finalFormData = { name: formData.name, color: "" };

      if (formData.color === "") {
        const randomColor =
          colorSelectArray[Math.floor(Math.random() * colorSelectArray.length)];

        console.log(randomColor);

        finalFormData.color = randomColor;
      } else {
        finalFormData.color = formData.color;
      }

      const newObject = { ...finalFormData, chats: [], id: uniqueKey };
      parsedData.push(newObject);
      localStorage.setItem("Data", JSON.stringify(parsedData));

      setBlur(false);

      const rerender_list = localStorage.getItem("Data");
      const rerenderparsedData = JSON.parse(rerender_list);
      setInfos(rerenderparsedData);
    }
  };

  const handleChange = (e) => {
    setInpBorder(true);
    const { value, name } = e.target;

    if (name === "color") {
      setFormData((prevData) => ({
        ...prevData,
        color: value,
      }));
    } else {
      setFormData((prevData) => ({
        ...prevData,
        name: value,
      }));
    }
  };

  return (
    <div
      style={{
        backgroundColor: blur ? "rgba(47, 47, 47, 0.75)" : "",
        position: "absolute",
      }}
      id={mlp.mlpMainBlurConatianer}>
      <div
        id={mlp.mlpCreateNewGrpBox}
        style={{
          display: blur ? "flex" : "none",
          position: "absolute",
          top: "12rem",
          left: "1.5rem",
          backgroundColor: "#FFFFFF",
          height: "10rem",
          width: "calc(100% - 3rem)",
          borderRadius: "8px",
          zIndex: "2",
        }}>
        <div id={mlp.mlpCreateBox}>
          <button
            id={mlp.mlpCreateBoxCrossButton}
            onClick={() => setBlur(false)}>
            X
          </button>
          <div id={mlp.mlpCreateBoxHeading}>Create New Notes group</div>

          <div id={mlp.mlpCreateBoxGrpName}>
            <p id={mlp.mlpCreateBoxGrpNameP}>Group Name</p>
            <input
              style={{
                borderColor: InpBorder ? "rgba(204, 204, 204, 1)" : "red",
              }}
              type='text'
              name='mlpCreateBoxGrpNameinp'
              value={formData.name}
              placeholder='Enter your group name....'
              id={mlp.mlpCreateBoxGrpNameinp}
              onChange={handleChange}
              required
            />
          </div>

          <div id={mlp.mlpCreateBoxGrpColor}>
            <p id={mlp.mlpCreateBoxGrpColorP}>Choose colour</p>
            {colorSelectArray.map((CurrColor) => (
              <div
                className={mlp.mlpCreateBoxGrpColorCircles}
                style={{
                  backgroundColor: CurrColor,
                  borderRadius: "50%",
                  height: "1rem",
                  width: "1rem",
                  cursor: "pointer",
                }}
                key={CurrColor}
                onClick={() =>
                  handleChange({ target: { value: CurrColor, name: "color" } })
                }></div>
            ))}
          </div>

          <div id={mlp.mlpCreateBoxCreateBtnDiv}>
            <button
              type='submit'
              id={mlp.mlpCreateBoxCreateBtn}
              onClick={handleFormSubmit}>
              Create
            </button>
          </div>
        </div>
      </div>

      <div id={mlp.mlpMainContainer}>
        <div id={mlp.mlpHeading}>Pocket Notes</div>
        <div
          id={mlp.mlpCrtNewGrp}
          style={{ opacity: blur ? "0.5" : "1" }}
          onClick={() => {
            setBlur(true);
          }}>
          + &nbsp;&nbsp;Create Notes group
        </div>
        <div id={mlp.mlpAllGrps}>
          {infos.map((info) => (
            <Link
              to={`/Chat/${info.id}`}
              key={info.id}
              className={mlp.mlpGrpBox}>
              {
                <div
                  className={mlp.mlpGrpBoxCircle}
                  style={{
                    backgroundColor: info.color,
                    borderRadius: "50%",
                    height: "3rem",
                    width: "3rem",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    justifySelf: "flex-start",
                    fontSize: "1.6rem",
                    color: "#FFFFFF",
                    opacity: blur ? "0.5" : "1",
                  }}>
                  {info.name
                    .split(" ")
                    .map((w) => w[0])
                    .join("")
                    .toUpperCase()}
                </div>
              }{" "}
              <p className={mlp.mlpGrpBoxP}>{info.name}</p>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
}

export default MobileLandingPage;
