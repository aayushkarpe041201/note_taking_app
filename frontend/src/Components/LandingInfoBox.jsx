import lib from "../css/LandingInfoBox.module.css";

function LandingInfoBox() {
  return (
    <div id={lib.mainBoxLib}>
      <div id={lib.libinfoNimg}>
        <img
          id={lib.libimg}
          src={"public\\assets\\image-removebg-preview 1.png"}
          alt='img'
        />
        <p id={lib.libHeading}>Pocket Notes</p>
        <p id={lib.libinfodata}>
          Send and receive messages without keeping your phone online.
          <br />
          Use Pocket Notes on up to 4 linked devices and 1 mobile phone
        </p>
      </div>
      <p id={lib.libfooter}>
        <span style={{ color: "transparent", textShadow: "0 0 0 black" }}>
          &#128274;
        </span>
        end-to-end encrypted
      </p>
    </div>
  );
}

export default LandingInfoBox;
