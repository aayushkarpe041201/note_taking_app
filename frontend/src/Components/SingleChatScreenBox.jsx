import { useCallback, useEffect, useState } from "react";
import scsb from "../css/SingleChatScreenBox.module.css";
import { Link, useNavigate, useParams } from "react-router-dom";
import SingleMsgBox from "./SingleMsgBox";
import { v4 as uuidv4 } from "uuid";

function SingleChatScreenBox() {
  const { id } = useParams();

  // eslint-disable-next-line no-unused-vars
  const [infos, setInfos] = useState([]);
  const [currInfo, setCurrInfo] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [newChat, setNewChat] = useState({
    text: "",
  });

  const navigate = useNavigate();

  useEffect(() => {
    const data = localStorage.getItem("Data");

    if (data) {
      const parsedData = JSON.parse(data);
      setInfos(parsedData);

      const currObj = parsedData.find((obj) => obj.id === id);
      setCurrInfo(currObj);
      setIsLoading(false);
    } else if (!id || !data) {
      navigate("/");
    }
  }, [id, navigate]);

  const handleSubmitMsg = useCallback(() => {
    if (newChat && newChat.text.length > 0) {
      const currentDate = new Date();

      const timeFormatOptions = {
        hour: "numeric",
        minute: "2-digit",
        hour12: true,
      };
      const formattedTime = currentDate.toLocaleTimeString(
        "en-US",
        timeFormatOptions
      );

      const dateFormatOptions = {
        day: "numeric",
        month: "long",
        year: "numeric",
      };
      const formattedDate = currentDate.toLocaleDateString(
        "en-US",
        dateFormatOptions
      );

      const editDate = formattedDate.split(" ");
      const finalDate = [
        editDate[1].slice(0, -1),
        editDate[0],
        editDate[2],
      ].join(" ");

      const data = {
        ...newChat,
        date: finalDate,
        time: formattedTime,
        _id: uuidv4(),
      };

      let local_Datas = localStorage.getItem("Data");
      let parsed_data = JSON.parse(local_Datas);

      let updated_Data = parsed_data.map((obj) => {
        if (obj.id === id) {
          return {
            ...obj,
            chats: [...obj.chats, data],
          };
        }
        return obj;
      });

      localStorage.setItem("Data", JSON.stringify(updated_Data));

      const updatedCurrInfo = updated_Data.find((obj) => obj.id === id);
      setCurrInfo(updatedCurrInfo);

      setNewChat({
        text: "",
      });
    }
  }, [newChat, id]);

  useEffect(() => {
    const handleKeyPress = (event) => {
      if (event.key === "Enter") {
        event.preventDefault();
        handleSubmitMsg();
      }
    };
    document.addEventListener("keydown", handleKeyPress);
    return () => {
      document.removeEventListener("keydown", handleKeyPress);
    };
  }, [handleSubmitMsg]);

  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div id={scsb.scsbMainBox}>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <>
          {/* Header */}
          <div id={scsb.scsbHeader}>
            <>
              {isMobile ? (
                <Link
                  to={"/"}
                  id={scsb.scsbHeaderGoTOHomePage}>
                  &larr;
                </Link>
              ) : (
                ""
              )}
            </>
            <div
              id={scsb.scsbHeaderCircle}
              style={{
                backgroundColor: currInfo.color,
                borderRadius: "50%",
                height: "3rem",
                width: "3rem",
                textAlign: "center",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                justifySelf: "flex-start",
                marginLeft: isMobile ? "0rem" : "0.8rem",
                fontSize: "1.22rem",
                color: "#FFFFFF",
              }}>
              {currInfo.name
                .split(" ")
                .map((w) => w[0])
                .join("")
                .toUpperCase()}
            </div>
            <div id={scsb.scsbHeaderP}>{currInfo.name}</div>
          </div>
          {/* All Chats Section */}
          <div id={scsb.scsbAllChatsSection}>
            {currInfo.chats.map((chat) => (
              <SingleMsgBox
                key={chat._id}
                chatObj={chat}
              />
            ))}
          </div>
          {/* Input Section */}
          <div id={scsb.scsbTextInpSection}>
            <div id={scsb.scsbTextInpSectionContainer}>
              <textarea
                rows='3'
                id={scsb.scsbTextInpSectionInp}
                placeholder='Enter your text here...........'
                value={newChat.text}
                onChange={(e) =>
                  setNewChat({ ...newChat, text: e.target.value })
                }></textarea>

              <svg
                xmlns='http://www.w3.org/2000/svg'
                width='35'
                height='29'
                viewBox='0 0 35 29'
                fill='none'
                id={scsb.scsbTextInpSectionBtn}
                onClick={handleSubmitMsg}>
                <path
                  d='M0 29V18.125L14.5 14.5L0 10.875V0L34.4375 14.5L0 29Z'
                  fill='#ABABAB'
                />
              </svg>
            </div>
          </div>
        </>
      )}
    </div>
  );
}

export default SingleChatScreenBox;
