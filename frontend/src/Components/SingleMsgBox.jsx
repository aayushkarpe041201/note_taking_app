import smb from "../css/SingleMsgBox.module.css";
import PropTypes from "prop-types";

function SingleMsgBox({ chatObj }) {
  return (
    <div id={smb.smbMainBox}>
      <div id={smb.smbMainBoxDateTime}>
        {chatObj.time}
        <br />
        {chatObj.date}
      </div>
      <div id={smb.smbMainBoxTxt}>{chatObj.text}</div>
    </div>
  );
}

SingleMsgBox.propTypes = {
  chatObj: PropTypes.object.isRequired,
};

export default SingleMsgBox;
