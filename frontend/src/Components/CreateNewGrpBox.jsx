import { v4 as uuidv4 } from "uuid";

import cngb from "../css/CreateNewGrpBox.module.css";
import { useCallback, useEffect, useState } from "react";
import PropTypes from "prop-types";

function CreateNewGrpBox({ isVisible, handleOnClickCNGB }) {
  const [InpBorder, setInpBorder] = useState(true);

  const [formData, setFormData] = useState({
    name: "",
    color: "",
  });

  const colorSelectArray = [
    "#B38BFA",
    "#FF79F2",
    "#43E6FC",
    "#F19576",
    "#0047FF",
    "#6691FF",
  ];

  const [cngbMainBoxBool, setcngbMainBoxBool] = useState(false);
  const [cngbCreateBox, setcngbCreateBox] = useState(false);

  const change_val = useCallback(() => {
    setFormData({
      name: "",
      color: "",
    });
    handleOnClickCNGB(false);
  }, [handleOnClickCNGB]);

  useEffect(() => {
    if (!cngbCreateBox && cngbMainBoxBool) {
      change_val();
    }
    return () => {
      setcngbMainBoxBool(false);
      setcngbCreateBox(false);
    };
  }, [cngbMainBoxBool, change_val, cngbCreateBox]);

  const handleFormSubmit = () => {
    const existingData = localStorage.getItem("Data");

    const parsedData = existingData ? JSON.parse(existingData) : [];

    const duplicateObject = parsedData.find(
      (obj) => obj.name === formData.name
    );

    if (duplicateObject) {
      setInpBorder(false);
    } else {
      const uniqueKey = uuidv4();

      const finalFormData = { name: formData.name, color: "" };

      if (formData.color === "") {
        const randomColor =
          colorSelectArray[Math.floor(Math.random() * colorSelectArray.length)];

        console.log(randomColor);

        finalFormData.color = randomColor;
      } else {
        finalFormData.color = formData.color;
      }

      const newObject = { ...finalFormData, chats: [], id: uniqueKey };
      parsedData.push(newObject);
      localStorage.setItem("Data", JSON.stringify(parsedData));

      change_val();
    }
  };

  const handleChange = (e) => {
    setInpBorder(true);
    const { value, name } = e.target;

    if (name === "color") {
      setFormData((prevData) => ({
        ...prevData,
        color: value,
      }));
    } else {
      setFormData((prevData) => ({
        ...prevData,
        name: value,
      }));
    }
  };

  return (
    <div
      id={cngb.cngbMainBox}
      style={{ display: isVisible ? "flex" : "none" }}
      onClick={() => {
        setcngbMainBoxBool(true);
      }}>
      <div
        id={cngb.cngbCreateBox}
        onClick={() => {
          setcngbCreateBox(true);
        }}>
        <div id={cngb.cngbCreateBoxHeading}>Create New Notes group</div>

        <div id={cngb.cngbCreateBoxGrpName}>
          <p id={cngb.cngbCreateBoxGrpNameP}>Group Name</p>
          <input
            style={{
              borderColor: InpBorder ? "rgba(204, 204, 204, 1)" : "red",
            }}
            type='text'
            name='cngbCreateBoxGrpNameinp'
            value={formData.name}
            placeholder='Enter your group name....'
            id={cngb.cngbCreateBoxGrpNameinp}
            onChange={handleChange}
            required
          />
        </div>

        <div id={cngb.cngbCreateBoxGrpColor}>
          <p id={cngb.cngbCreateBoxGrpColorP}>Choose colour</p>
          {colorSelectArray.map((CurrColor) => (
            <div
              className={cngb.cngbCreateBoxGrpColorCircles}
              style={{
                backgroundColor: CurrColor,
                borderRadius: "50%",
                height: "2rem",
                width: "2rem",
                cursor: "pointer",
              }}
              key={CurrColor}
              onClick={() =>
                handleChange({ target: { value: CurrColor, name: "color" } })
              }></div>
          ))}
        </div>

        <div id={cngb.cngbCreateBoxCreateBtnDiv}>
          <button
            type='submit'
            id={cngb.cngbCreateBoxCreateBtn}
            onClick={handleFormSubmit}>
            Create
          </button>
        </div>
      </div>
    </div>
  );
}

CreateNewGrpBox.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  handleOnClickCNGB: PropTypes.func.isRequired,
};

export default CreateNewGrpBox;
