import MobileLandingPage from "../Components/MobileLandingPage";

import { BrowserRouter, Route, Routes } from "react-router-dom";
import SingleChatScreenBox from "../Components/SingleChatScreenBox";

function MobilePageScreen() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path='/'
          element={<MobileLandingPage />}
        />
        <Route
          path='/Chat/:id'
          element={<SingleChatScreenBox />}
        />
      </Routes>
    </BrowserRouter>
  );
}

export default MobilePageScreen;
