import { useState } from "react";

import { BrowserRouter, Route, Routes } from "react-router-dom";

import wps from "../css/WebPageScreen.module.css";
import LandingBox from "../Components/LandingBox";
import LandingInfoBox from "../Components/LandingInfoBox";
import CreateNewGrpBox from "../Components/CreateNewGrpBox";
import SingleChatScreenBox from "../Components/SingleChatScreenBox";

function WebPageScreen() {
  const [CreateNewGrpBoxVisile, setCreateNewGrpBoxVisile] = useState(false);

  const handleOnClickNewGrpBox = (boolVal) => {
    setCreateNewGrpBoxVisile(boolVal);
  };

  const handleOnClickCNGB = (boolVal) => {
    setCreateNewGrpBoxVisile(boolVal);
  };

  return (
    <BrowserRouter>
      <div id={wps.parentWps}>
        <div id={wps.mainBoxWps}>
          <LandingBox handleOnClickNewGrpBox={handleOnClickNewGrpBox} />
          <Routes>
            <Route
              path='/'
              element={<LandingInfoBox />}
            />
            <Route
              path='/Chat/:id'
              element={<SingleChatScreenBox  />}
            />
          </Routes>
        </div>
        <CreateNewGrpBox
          isVisible={CreateNewGrpBoxVisile}
          handleOnClickCNGB={handleOnClickCNGB}
        />
      </div>
    </BrowserRouter>
  );
}

export default WebPageScreen;
